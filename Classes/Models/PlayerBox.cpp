#include "PlayerBox.h"
#include "Managers/SceneManager.h"
#include "Managers/GameManager.h"

USING_NS_CC;

PlayerBox::PlayerBox():    
    _listener(nullptr),
    _health(3),
    _textureName("player1_") {
}

PlayerBox *PlayerBox::create(const std::string& filename) {
    PlayerBox *playerBox = new (std::nothrow) PlayerBox();
    if (playerBox && playerBox->initWithSpriteFrameName(filename)) {
        playerBox->autorelease();
        return playerBox;
    }

    CC_SAFE_DELETE(playerBox);
    return nullptr;
}

void PlayerBox::onEnter() {
    Sprite::onEnter();

    _listener = cocos2d::EventListenerTouchOneByOne::create();

    _listener->onTouchBegan = CC_CALLBACK_2(PlayerBox::onTouchBegan, this);
    _listener->onTouchMoved = CC_CALLBACK_2(PlayerBox::onTouchMoved, this);
    _listener->onTouchEnded = CC_CALLBACK_2(PlayerBox::onTouchEnded, this);

    auto dispatcher = cocos2d::Director::getInstance()->getEventDispatcher();
    dispatcher->addEventListenerWithSceneGraphPriority(_listener, this);
}

bool PlayerBox::onTouchBegan(Touch *touch, Event *unused_event) {

    //    std::string str = ;

    if(getAnchorPoint() == Point::ANCHOR_BOTTOM_LEFT) {
        setAnchorPoint(Point::ANCHOR_BOTTOM_RIGHT);
        //        setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("pblack.png"));        
        this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(_textureName + "black.png"));
    }
    else {
        setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(_textureName + "white.png"));
    }

    return true;
}

void PlayerBox::onTouchMoved(Touch *touch, Event *unused_event) {
}

void PlayerBox::onTouchEnded(Touch *touch, Event *unused_event) {
}

void PlayerBox::setEnabled(bool flag) {
    _listener->setEnabled(flag);
}

const bool &PlayerBox::getEnabled() const {
    return _listener->isEnabled();
}

PLAYER_BOX_STATE PlayerBox::processCollision(const Box *box) {
    if (box->getBoxType() == BOX_TYPE::HEART) {
        if (_health == 3) {

            auto callback1 = CallFunc::create([this](){
                _textureName = "plus_heart1_";
                if (_anchorPoint == Point::ANCHOR_BOTTOM_LEFT) {
                    this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(_textureName + "white.png"));
                } else {                    
                    this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(_textureName + "black.png"));
                }
            });

            auto callback2 = CallFunc::create([this](){
                _textureName = "player1_";
                if (_anchorPoint == Point::ANCHOR_BOTTOM_LEFT) {                    
                    this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(_textureName + "white.png"));
                } else {                    
                    this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(_textureName + "black.png"));
                }
            });

            this->runAction(Sequence::create(callback1, DelayTime::create(3.f), callback2, nullptr));
        } else {
            ++_health;
            auto callback1 = CallFunc::create([this](){
                _textureName = "plus_heart2_";
                if (_anchorPoint == Point::ANCHOR_BOTTOM_LEFT) {                    
                    this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(_textureName + "white.png"));
                } else {
                    this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(_textureName + "black.png"));
                }
            });

            auto callback2 = CallFunc::create([this](){
                if (_health == 2) {
                    _textureName = "player2_";
                } else {                    
                    _textureName = "player1_";
                }
                if (_anchorPoint == Point::ANCHOR_BOTTOM_LEFT) {
                    this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(_textureName + "white.png"));
                } else {
                    this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(_textureName + "black.png"));
                }
            });            

            this->runAction(Sequence::create(callback1, DelayTime::create(1.5f), callback2, nullptr));
        }
    } else {
        if (_health == 1) {

            auto callback1 = CallFunc::create([this](){
                _textureName = "lose1_";
                if (_anchorPoint == Point::ANCHOR_BOTTOM_LEFT) {                    
                    this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(_textureName + "white.png"));
                } else {                    
                    this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(_textureName + "black.png"));
                }
            });

            auto callback2 = CallFunc::create([this](){
                _textureName = "lose2_";
                if (_anchorPoint == Point::ANCHOR_BOTTOM_LEFT) {                    
                    this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(_textureName + "white.png"));
                } else {
                    this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(_textureName + "black.png"));
                }
            });

            auto callback3 = CallFunc::create([this](){
                _textureName = "lose3_";
                if (_anchorPoint == Point::ANCHOR_BOTTOM_LEFT) {                    
                    this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(_textureName + "white.png"));
                } else {                    
                    this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(_textureName + "black.png"));
                }
            });

            this->runAction(RepeatForever::create(Sequence::create(callback1, DelayTime::create(1.f), callback2,
                                                                   DelayTime::create(1.f), callback3, DelayTime::create(1.f), nullptr)));

            return PLAYER_BOX_STATE::GAMEOVER;
        } else {


            if (_health == 2) {
                _textureName = "player3_";
            } else {
                _textureName = "player2_";
            }
            if (_anchorPoint == Point::ANCHOR_BOTTOM_LEFT) {                
                this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(_textureName + "white.png"));
            } else {                
                this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(_textureName + "black.png"));
            }


            --_health;
        }
    }

    return PLAYER_BOX_STATE::OK;
}


