#include "Box.h"
#include "../Utils.h"
//#include <cstdio>

USING_NS_CC;

Box::Box():
    _boxColor(BOX_COLOR::BLACK_BOX),
    _boxType(BOX_TYPE::SKULL){
}

void Box::move(float dt) {
    setPositionY(getPositionY() - dt);
}

void Box::setAttributes(BOX_TYPE boxType, BOX_COLOR boxColor) {
    _boxType = boxType;
    _boxColor = boxColor;

    std::string textureName = "box_";

    switch (boxType) {
    case BOX_TYPE::HEART:
        textureName.append("heart_");
        break;
    default:
        textureName.append("skull_");
        break;
    }

    switch (boxColor) {
    case BOX_COLOR::WHITE_BOX:
        textureName.append("white.png");
        this->setAnchorPoint(Point::ANCHOR_BOTTOM_LEFT);
        break;
    default:
        textureName.append("black.png");
        this->setAnchorPoint(Point::ANCHOR_BOTTOM_RIGHT);
        break;
    }

    this->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(textureName));
}

const BOX_COLOR &Box::getBoxColor() const {
    return _boxColor;
}

const BOX_TYPE &Box::getBoxType() const {
    return _boxType;
}

Box *Box::create(const std::string &filename) {
    Box *box = new (std::nothrow) Box();
    if (box && box->initWithSpriteFrameName(filename)) {
        box->autorelease();
        return box;
    }
    CC_SAFE_DELETE(box);
    return nullptr;
}
