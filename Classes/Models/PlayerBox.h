#ifndef __PLAYERBOX_H__
#define __PLAYERBOX_H__

#include "Box.h"
#include "cocos2d.h"

//emotions
enum class PLAYER_BOX_STATE {
    OK = 0,
    GAMEOVER
};

class PlayerBox : public cocos2d::Sprite {
public:
    PlayerBox();

    static PlayerBox *create(const std::string& filename);

    void onEnter();
    virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event) final;
    virtual void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event) final;
    virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event) final;

    void setEnabled(bool flag);
    const bool &getEnabled() const;

    PLAYER_BOX_STATE processCollision(const Box *box);
private:
    cocos2d::EventListenerTouchOneByOne* _listener;    
    BOX_COLOR _boxColor;
    int _health;
    std::string _textureName;
};

#endif // __PLAYERBOX_H__
