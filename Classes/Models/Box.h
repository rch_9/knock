#ifndef __BOX_H__
#define __BOX_H__

#include "cocos2d.h"

enum class BOX_COLOR {
    BLACK_BOX = 0,
    WHITE_BOX
};

enum class BOX_TYPE {
    SKULL = 0,
    HEART
};

class Box : public cocos2d::Sprite {
public:
    Box();  

    static Box *create(const std::string &filename);      

    void move(float dt);

    void setAttributes(BOX_TYPE boxType, BOX_COLOR boxColor);

    const BOX_COLOR &getBoxColor() const;
    const BOX_TYPE &getBoxType() const;

private:
    BOX_TYPE _boxType;
    BOX_COLOR _boxColor;
};

#endif // __BOX_H__

