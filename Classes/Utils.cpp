#include "Utils.h"

//using namespace std;

USING_NS_CC;


//    example
//    auto a = static_cast<__String*>(dictionary->objectForKey("current"));
//    CCLOG("%s", a->getCString());

__Dictionary *KnockUtils::getDictionary() {
    auto sharedFileUtils = FileUtils::getInstance();
    std::string fullPath = sharedFileUtils->getWritablePath() + DATABASE_NAME;

    //    __Dictionary* dictionary = nullptr;

    //    if (sharedFileUtils->isFileExist(fullPath.c_str())) {
    //        dictionary = __Dictionary::createWithContentsOfFile(fullPath.c_str());
    //    } else {
    //        initDictionary(dictionary, fullPath.c_str());
    //    }

    return __Dictionary::createWithContentsOfFile(fullPath.c_str());;
}

bool KnockUtils::initDictionary() {
    auto sharedFileUtils = FileUtils::getInstance();
    std::string fullPath = sharedFileUtils->getWritablePath() + DATABASE_NAME;

    if (!sharedFileUtils->isFileExist(fullPath.c_str())) {
        auto dictionary = __Dictionary::create();

        dictionary->setObject(__Integer::create(0), "max_level");        
        dictionary->setObject(__Bool::create(1), "music");
        dictionary->setObject(__Bool::create(1), "vibro");

        dictionary->writeToFile(fullPath.c_str());
    }

    return true;
}
