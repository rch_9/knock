#include "SoundManager.h"
#include "DictionaryManager.h"
#include "SimpleAudioEngine.h"
#include "cocos2d.h"
#include "../Utils.h"

USING_NS_CC;

SoundManager *SoundManager::getInstance() {
    static SoundManager instance;
    return &instance;
}

SoundManager::SoundManager():
    _isMusicPlaying(false),
    _isBellPlaying(false),
    _isVibrationPlaying(false) {
}

bool SoundManager::init() {
    auto audio = CocosDenshion::SimpleAudioEngine::getInstance();

    audio->preloadBackgroundMusic("full-5.ogg");

    _isMusicPlaying = KnockUtils::converStringToBool(DictionaryManager::getInstance()->get("music"));
    _isVibrationPlaying = KnockUtils::converStringToBool(DictionaryManager::getInstance()->get("vibro"));    

    return true;
}

void SoundManager::playBackgroundMusic() {
    auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
    audio->playBackgroundMusic("full-5.ogg", false);

    if (!_isMusicPlaying) {
//        audio->pauseBackgroundMusic();
        audio->setBackgroundMusicVolume(0.f);
    }
}

void SoundManager::pauseBackgroundMusic() {
    CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

void SoundManager::stopBackgroundMusic() {
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
}

void SoundManager::resumeBackgroundMusic() {
    CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}

void SoundManager::playEffect() {
    if (_isVibrationPlaying) {
        cocos2d::Device::vibrate(0.1);
    }
}

// FIXME
void SoundManager::toggleMusicPlaying() {
    _isMusicPlaying = !_isMusicPlaying;
    auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
    if (!_isMusicPlaying) {
        audio->setBackgroundMusicVolume(0.f);
    } else {
        audio->setBackgroundMusicVolume(1.f);
    }    
    DictionaryManager::getInstance()->put(__Bool::create(_isMusicPlaying), "music");
}

void SoundManager::toggleVibrationPlaying() {
    _isVibrationPlaying = !_isVibrationPlaying;
    DictionaryManager::getInstance()->put(__Bool::create(_isVibrationPlaying), "vibro");
}

const bool &SoundManager::getIsMusicPlaying() const {
    return _isMusicPlaying;
}

const bool &SoundManager::getIsVibrationPlaying() const {
    return _isVibrationPlaying;
}

