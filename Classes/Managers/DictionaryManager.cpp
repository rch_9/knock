#include "DictionaryManager.h"
#include "cocos2d.h"
#include <string>
#include <fstream>
#include "../Utils.h"

USING_NS_CC;

DictionaryManager *DictionaryManager::getInstance() {
    static DictionaryManager instance;
    return &instance;
}

DictionaryManager::DictionaryManager():
    databasePath("db.plist") {
}

bool DictionaryManager::init() {
    KnockUtils::initDictionary();    

    auto sharedFileUtils = FileUtils::getInstance();
    std::string fullPath = sharedFileUtils->getWritablePath() + databasePath;
    databasePath = const_cast<char*>(fullPath.c_str());

    databasePath.insert(databasePath.begin(), fullPath.begin(), fullPath.begin());

    return true;
}

const char* DictionaryManager::get(const char *request) {
    __Dictionary* dictionary = KnockUtils::getDictionary();

    return static_cast<__String*>(dictionary->objectForKey(request))->getCString();
}

void DictionaryManager::put(Ref *data, const char *key) {
    __Dictionary* dictionary = KnockUtils::getDictionary();

    dictionary->setObject(data, key);
    dictionary->writeToFile(databasePath.c_str());
}
