#ifndef __DICTIONARYMANAGER_H__
#define __DICTIONARYMANAGER_H__

#include "BasicManager.h"
#include "utility"
#include "cocos2d.h"
#include "string"

class DictionaryManager : public BasicManager {
public:
    static DictionaryManager* getInstance();
    bool init() override;

    const char* get(const char* request);

    void put(cocos2d::Ref *data, const char* key);
private:
    DictionaryManager();
    std::string databasePath;
//    char* databasePahs
};

#endif // __DICTIONARYMANAGER_H__

