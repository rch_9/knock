#ifndef __SCENEMANAGER_H__
#define __SCENEMANAGER_H__

#include "BasicManager.h"
#include "Scenes/GameScene.h"

class SceneManager : public BasicManager {
public:
    static SceneManager* getInstance();
    bool init() override;

//    may?
    void goToPauseScene();
    void goToMainMenuScene();
    void goToGameScene();
    void goToGameSceneFromPauseScene(cocos2d::Ref *sender);    
    void goToGameOverScene();
    void goToRestart();
    void goToGameSceneFromGameOverScene();
    void goToSettingsScene(cocos2d::Ref *sender);
    void goFromSettingsScene(cocos2d::Ref *sender);
    void goToVictoryScene();
    void goToQuit();

    GameLayer *getGameLayer() const;

private:
    SceneManager();

    GameLayer* _gameLayer;

//    SceneManager() = delete;
//    SceneManager(SceneManager&) = delete;
//    SceneManager(SceneManager&&) = delete;
//    SceneManager operator= ( const class_name & ) = delete;
};

#endif // __SCENEMANAGER_H__

