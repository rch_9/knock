#ifndef __BASICMANAGER_H__
#define __BASICMANAGER_H__

class BasicManager {
public:
    virtual bool init() = 0;
    virtual inline void onAppPause() { }
    virtual inline void onAppResume() { }
    virtual inline ~BasicManager() { }
};

#endif // __BASICMANAGER_H__
