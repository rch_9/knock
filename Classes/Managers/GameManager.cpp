#include "GameManager.h"
#include "SceneManager.h"
#include "Scenes/MainMenuScene.h"
#include "SdkboxManager.h"
#include "cocos2d.h"
#include "SoundManager.h"
#include "DictionaryManager.h"
#include <utility>
#include <cmath>

USING_NS_CC;

GameManager::GameManager():
    _lastBoxType(BOX_COLOR::WHITE_BOX),
    _amountConsecutiveSameBoxes(0) {
}

void GameManager::reset() {
    _lastBoxType = BOX_COLOR::WHITE_BOX;
    _amountConsecutiveSameBoxes = 0;
}

GameManager *GameManager::getInstance() {
    static GameManager instance;
    return &instance;
}

bool GameManager::init() {    
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("game.plist", "game.png");


    DictionaryManager::getInstance()->init();
    SoundManager::getInstance()->init();

    _maxLevel = KnockUtils::converStringToInt(DictionaryManager::getInstance()->get("max_level"));
    SdkboxManager::getInstance()->submitScore(_maxLevel);

    for (int i = 1; i < _maxLevel; ++i) {
        SdkboxManager::getInstance()->unlockAchievement(i);
    }

    return true;
}

const int &GameManager::getMaxLevel() const {
    return _maxLevel;
}

void GameManager::putLevel(const int &level) {
#ifdef SDKBOX_ENABLED
    SdkboxManager::getInstance()->unlockAchievement(level);
#endif
    if (_maxLevel < level) {
        _maxLevel = level;
        SdkboxManager::getInstance()->submitScore(level);
        DictionaryManager::getInstance()->put(__Integer::create(_maxLevel), "max_level");
    }
}

void GameManager::processCollisions(const Box* box) {
    SoundManager::getInstance()->playEffect();

    switch (SceneManager::getInstance()->getGameLayer()->_playerBox->processCollision(box)) {
    case PLAYER_BOX_STATE::OK:
        break;
    default:
        SceneManager::getInstance()->goToGameOverScene();
        break;
    }
}

std::pair<BOX_TYPE, BOX_COLOR> GameManager::generateNextBoxAttributes() {
    auto boxColor = static_cast<BOX_COLOR>(RandomHelper::random_int(0, 1));
    auto boxType = static_cast<BOX_TYPE>(RandomHelper::random_int(0, 3) == 0 ? 1 : 0);
    if (_lastBoxType == boxColor) {
        ++_amountConsecutiveSameBoxes;

        if (_amountConsecutiveSameBoxes == 3) {
            if (boxColor == BOX_COLOR::BLACK_BOX) {
                boxColor = BOX_COLOR::WHITE_BOX;
                _lastBoxType = BOX_COLOR::WHITE_BOX;
            } else {
                boxColor = BOX_COLOR::BLACK_BOX;
                _lastBoxType = BOX_COLOR::BLACK_BOX;
            }
            _amountConsecutiveSameBoxes = 0;
        }
    } else {
        _lastBoxType = boxColor;
        _amountConsecutiveSameBoxes = 0;
    }

    return std::make_pair(boxType, boxColor);
}
