#include "SceneManager.h"
#include "Scenes/MainMenuScene.h"
#include "Scenes/PauseScene.h"
#include "Scenes/GameOverScene.h"
#include "Scenes/SettingsScene.h"
#include "Scenes/VictoryScene.h"
#include "Managers/GameManager.h"
#include "Managers/SoundManager.h"
#include "SdkboxManager.h"
#include "cocos2d.h"
#include "Utils.h"

USING_NS_CC;

SceneManager* SceneManager::getInstance() {
    static SceneManager instance;
    return &instance;
}

SceneManager::SceneManager():
    _gameLayer(nullptr) {
}

GameLayer *SceneManager::getGameLayer() const {
    return _gameLayer;
}

bool SceneManager::init() {
    return true;
}

void SceneManager::goToPauseScene() {
    SoundManager::getInstance()->pauseBackgroundMusic();
    SdkboxManager::getInstance()->showBanner();

    if (_gameLayer && _gameLayer->_playerBox->getEnabled()) {
        _gameLayer->getChildByTag(2)->setVisible(false); // 2 - pause button

        auto layer = _gameLayer->getChildByTag(0);
        layer->stopAllActions();
        layer->runAction(RotateTo::create(1.f, Vec3(0.f, 0.f, 0.f)));

        _gameLayer->addChild(PauseLayer::create(), 11, 4); // 4 - pause layer
        _gameLayer->pause();
        _gameLayer->setEnabledPlayerBox(false);
    }
}

void SceneManager::goToMainMenuScene() {
    Director::getInstance()->popToRootScene();
    _gameLayer->getParent()->removeAllChildren();
}

void SceneManager::goToGameScene() {
    //music
    SdkboxManager::getInstance()->hideBanner();
    SoundManager::getInstance()->playBackgroundMusic();
    auto scene = GameLayer::createScene();
    _gameLayer = static_cast<GameLayer*>(scene->getChildByTag(0));
    Director::getInstance()->pushScene(TransitionFade::create(0.5, scene));
}

void SceneManager::goToGameSceneFromPauseScene(Ref *sender) {
    SdkboxManager::getInstance()->hideBanner();
    SoundManager::getInstance()->resumeBackgroundMusic();
    _gameLayer->getEventDispatcher()->resumeEventListenersForTarget(_gameLayer, true);
    _gameLayer->resume();
    _gameLayer->getChildByTag(2)->setVisible(true); // 2 - pause button
    _gameLayer->removeChildByTag(4); // 4 - pause layer
    _gameLayer->setEnabledPlayerBox(true);
}

void SceneManager::goToGameOverScene() {
    SoundManager::getInstance()->stopBackgroundMusic();
    SdkboxManager::getInstance()->showBanner();
    auto layer = _gameLayer->getChildByTag(0);
    layer->stopAllActions();
    layer->runAction(RotateTo::create(1.f, Vec3(0.f, 0.f, 0.f)));

    _gameLayer->getChildByTag(2)->setVisible(false); // 2 - pause button

    _gameLayer->addChild(GameOverLayer::create(), 11);
    _gameLayer->pause();
    _gameLayer->setEnabledPlayerBox(false);
}

void SceneManager::goToRestart() {
    this->goToMainMenuScene();
    GameManager::getInstance()->reset();
    this->goToGameScene();
}

void SceneManager::goToGameSceneFromGameOverScene() {

}

void SceneManager::goToSettingsScene(Ref *sender) {
    dynamic_cast<MenuItemImage*>(sender)->getParent()->getParent()->addChild(SettingsLayer::create(), 3);
    dynamic_cast<MenuItemImage*>(sender)->getParent()->setVisible(false);
}

void SceneManager::goFromSettingsScene(Ref *sender) {
    dynamic_cast<MenuItemImage*>(sender)->getParent()->getParent()->getParent()->getChildByTag(3)->setVisible(true); // 3 - menu
    dynamic_cast<MenuItemImage*>(sender)->getParent()->removeFromParent();
}

void SceneManager::goToVictoryScene() {
    SoundManager::getInstance()->stopBackgroundMusic();
    _gameLayer->getChildByTag(2)->setVisible(false); // 2 - pause button
    _gameLayer->pause();
    _gameLayer->setEnabledPlayerBox(false);
    _gameLayer->addChild(VictoryLayer::create(), 11);
}

void SceneManager::goToQuit() {
    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
