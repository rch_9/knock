#ifndef __GAMEMANAGER_H__
#define __GAMEMANAGER_H__

#include "BasicManager.h"
#include "Models/Box.h"
#include "utility"
#include "../Utils.h"
//#include "cocos2d.h"

class GameManager : public BasicManager
{
public:
    static GameManager* getInstance();
    bool init() override;

    void processCollisions(const Box *box);

    std::pair<BOX_TYPE, BOX_COLOR> generateNextBoxAttributes();
    void reset();

    const int &getMaxLevel() const;
    void putLevel(const int &level);
private:
    GameManager();

    BOX_COLOR _lastBoxType;
    int _amountConsecutiveSameBoxes;
    int _maxLevel;
};

#endif // __GAMEMANAGER_H__
