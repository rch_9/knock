// #if (CC_TARGET_PLATFORM != CC_PLATFORM_WIN32)
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
// #else
// #include <io.h>
// #include <WS2tcpip.h>
// #endif


#include "SdkboxManager.h"

#ifdef SDKBOX_ENABLED
using namespace sdkbox;

// SdkboxManager::SdkboxManager():
//     _iap(IAPListenerImpl()) {
// }
// #else
// SdkboxManager::SdkboxManager() {
// }
#endif

SdkboxManager* SdkboxManager::getInstance() {
    static SdkboxManager instance;
    return &instance;
}

bool SdkboxManager::init() {
#ifdef SDKBOX_ENABLED
    PluginSdkboxPlay::init();
    PluginSdkboxPlay::signin();

    PluginReview::init();

    PluginAppodeal::init();
    // PluginAppodeal::setDebugEnabled(true);
    PluginAppodeal::cacheAd(PluginAppodeal::AdType::AppodealAdTypeBanner);
    // PluginAppodeal::showAd(PluginAppodeal::ShowStyle::AppodealShowStyleBannerTop);

    // IAP::setDebug(true);
    // IAP::setListener(&_iap);
    // IAP::init();
#endif
}

// void SdkboxManager::purchase() {
// #ifdef SDKBOX_ENABLED
//     IAP::purchase("remove_ads");
// #endif
// }

void SdkboxManager::unlockAchievement(const int &level) const {
#ifdef SDKBOX_ENABLED
    std::string name;
    switch (level) {
      case 0:
          name = "Reassurance";
      break;
      case 1:
          name = "Stars path";
      break;
      case 2:
          name = "Darkaround";
      break;
      case 3:
          name = "Ultraspeeed";
      break;
      default:
          name = "Almost there";
      break;
    }

    PluginSdkboxPlay::unlockAchievement(name);
#endif
}

void SdkboxManager::submitScore(const int &score) const {
#ifdef SDKBOX_ENABLED
    PluginSdkboxPlay::submitScore("Hall of Fame", score);
#endif
}

void SdkboxManager::showBanner() {
#ifdef SDKBOX_ENABLED
    PluginAppodeal::showAd(PluginAppodeal::ShowStyle::AppodealShowStyleBannerTop);
#endif
}

void SdkboxManager::hideBanner() {
#ifdef SDKBOX_ENABLED
    PluginAppodeal::hideBanner();
#endif
}

void SdkboxManager::showLeaderboard() {
#ifdef SDKBOX_ENABLED
    PluginSdkboxPlay::showAllLeaderboards();
#endif
}

// #ifdef SDKBOX_ENABLED
// void IAPListenerImpl::onInitialized(bool ok) {
// }
//
// void IAPListenerImpl::onSuccess(Product const& p) {
//     // PluginAppodeal::hideBanner();
// }
//
// void IAPListenerImpl::onFailure(Product const& p, const std::string &msg) {
// }
//
// void IAPListenerImpl::onCanceled(Product const& p) {
// }
//
// void IAPListenerImpl::onRestored(Product const& p) {
// }
//
// void IAPListenerImpl::onProductRequestSuccess(std::vector<Product> const &products) {
// }
//
// void IAPListenerImpl::onProductRequestFailure(const std::string &msg) {
// }
//
// void IAPListenerImpl::onRestoreComplete(bool ok, const std::string &msg) {
// }
//
// #endif // SDKBOX_ENABLED
