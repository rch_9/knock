#ifndef __SDKBOXMANAGER_H__
#define __SDKBOXMANAGER_H__

#include "BasicManager.h"

#ifdef SDKBOX_ENABLED
#include "pluginreview/PluginReview.h"
#include "pluginappodeal/PluginAppodeal.h"
// #include "pluginiap/PluginIAP.h"
#include "pluginsdkboxplay/PluginSdkboxPlay.h"

// class IAPListenerImpl : public sdkbox::IAPListener {
//     virtual void onInitialized(bool ok) override;
//     virtual void onSuccess(sdkbox::Product const& p) override;
//     virtual void onFailure(sdkbox::Product const& p, const std::string &msg) override;
//     virtual void onCanceled(sdkbox::Product const& p) override;
//     virtual void onRestored(sdkbox::Product const& p) override;
//     virtual void onProductRequestSuccess(std::vector<sdkbox::Product> const &products) override;
//     virtual void onProductRequestFailure(const std::string &msg) override;
//     virtual void onRestoreComplete(bool ok, const std::string &msg) override;
// };
#endif // SDKBOX_ENABLED

class SdkboxManager : public BasicManager  {
public:
    bool init() override;

    static SdkboxManager* getInstance();
    // IAP
    // void purchase();
    void showLeaderboard();
//    void signin();
    void unlockAchievement(const int &level) const;
    void submitScore(const int &score) const;

    void showBanner();
    void hideBanner();
private:
// #ifdef SDKBOX_ENABLED
//     IAPListenerImpl _iap;
// #endif // SDKBOX_ENABLED
    // SdkboxManager();
};

#endif // __SDKBOXMANAGER_H__
