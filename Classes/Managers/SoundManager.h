#ifndef __SOUNDMANAGER_H__
#define __SOUNDMANAGER_H__

#include "BasicManager.h"

class SoundManager : public BasicManager {
public:
    static SoundManager* getInstance();
    bool init() override;

    void playBackgroundMusic();
    void pauseBackgroundMusic();
    void stopBackgroundMusic();
    void resumeBackgroundMusic();

    void playEffect();

    void toggleMusicPlaying();
    void toggleBellPlaying();
    void toggleVibrationPlaying();

    const bool &getIsMusicPlaying() const;
    const bool &getIsBellPlaying() const;
    const bool &getIsVibrationPlaying() const;

private:
    SoundManager();

    bool _isMusicPlaying;
    bool _isBellPlaying;
    bool _isVibrationPlaying;
};

#endif // __SOUNDMANAGER_H__
