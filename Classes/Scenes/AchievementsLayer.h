#ifndef __ACHIEVEMENTSLAYER_H__
#define __ACHIEVEMENTSLAYER_H__

#include "cocos2d.h"

class AchievementsLayer : public NS_CC::Layer {
public:
    virtual bool init() override;

    CREATE_FUNC(AchievementsLayer);
private:
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
};

#endif // __ACHIEVEMENTSLAYER_H__
