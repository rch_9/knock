#include "MainMenuScene.h"
//#include "GameScene.h"
#include "Managers/SceneManager.h"
#include "Managers/GameManager.h"
#include "Managers/SdkboxManager.h"
#include "AchievementsLayer.h"
#include "ui/CocosGUI.h"
#include "../Utils.h"

USING_NS_CC;

MainMenuLayer::MainMenuLayer() {
}


Scene *MainMenuLayer::createScene() {
    auto scene = Scene::create();
    auto layer = MainMenuLayer::create();
    scene->addChild(layer);

    return scene;
}


bool MainMenuLayer::init() {
    if (!Layer::init()) {
        return false;
    }

    setKeyboardEnabled(true);

    GameManager::getInstance()->init();

    auto visibleSize = Director::getInstance()->getVisibleSize();

    auto start = MenuItemImage::create("menu/play.png", "menu/play_selected.png", [&](Ref* sender) {
            SceneManager::getInstance()->goToGameScene();
});
    auto quit = MenuItemImage::create("menu/power.png", "menu/power_selected.png", [&](Ref* sender) {
            SceneManager::getInstance()->goToQuit();
});
    auto settings = MenuItemImage::create("menu/settings.png", "menu/settings_selected.png", [&](Ref* sender) {
        setKeyboardEnabled(false);
        SceneManager::getInstance()->goToSettingsScene(sender);        
});
    auto leaderboard = MenuItemImage::create("menu/achivments.png", "menu/achivments_selected.png", [&](Ref* sender) {
        setKeyboardEnabled(false);
        this->runAction(Sequence::create(CallFunc::create([this](){
            this->addChild(AchievementsLayer::create(), 10);
    }), DelayTime::create(0.1), CallFunc::create([this](){
        SdkboxManager::getInstance()->showLeaderboard();
    }), nullptr));
});

    auto menu = Menu::create(leaderboard, start, settings, quit, nullptr);
    menu->setPosition(visibleSize / 2.f);
    menu->alignItemsVerticallyWithPadding(visibleSize.height / 9.f);
    this->addChild(menu, 3, 3); // 3 - menu

    return true;
}

void MainMenuLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event) {
    if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE) {
        SceneManager::getInstance()->goToQuit();
    }
}
