#ifndef __VICTORYLAYER_H__
#define __VICTORYLAYER_H__

#include "cocos2d.h"

class VictoryLayer : public NS_CC::Layer {
public:
    virtual bool init() override;

    CREATE_FUNC(VictoryLayer);

private:    
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event) override;
};

#endif // __VICTORYLAYER_H__
