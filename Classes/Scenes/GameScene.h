#ifndef __GAMESCENE_SCENE_H__
#define __GAMESCENE_SCENE_H__

#include "cocos2d.h"
#include "Models/PlayerBox.h"
#include "Models/Box.h"
#include <deque>


class GameLayer : public cocos2d::Layer/*Color*/ {
public:
    GameLayer();

    static cocos2d::Scene* createScene();

    virtual bool init() override;       
    
    void setEnabledPlayerBox(bool flag);

    void generateBox();
    void processCollisions();
    void processTime(const float &dt);
    inline void moveBoxes(float dt);

    void update(float dt) override;

    CREATE_FUNC(GameLayer);   

    PlayerBox *_playerBox;
    void pressedPause(cocos2d::Ref *sender);    

    const int &getLevel() const;

private:
    inline void removeBox(std::deque<Box*>::iterator box);
    void setFactors(const int &p1, const int &p2, const int &p3, const int &p4, const float &p5, const float &p6, const int &p7, const int &p8, const float &p9);

    std::vector<Box*> _standingBoxes;
    std::deque<Box*>  _flyingBoxes;

    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event) override;

    cocos2d::Size _visibleSize;
    cocos2d::Vec2 _visibleOrigin;

    int _rotateAngleFactor[4]; // диапазоны вращений(по z берем 2 координаты)
    float _speedFactor; // скорость движения блоков
    int _distanceFactor[2]; // дистанции для блоков
    float _rotateTimeFactor[2]; //время на вращение

    float _time;
    int _level;
};

#endif // __GAMESCENE_H__
