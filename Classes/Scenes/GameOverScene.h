#ifndef __GAMEOVERLAYER_H__
#define __GAMEOVERLAYER_H__

#include "cocos2d.h"

class GameOverLayer : public NS_CC::Layer {
public:
    virtual bool init() override;   

    CREATE_FUNC(GameOverLayer);
private:
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
};

#endif // __GAMEOVERLAYER_H__

