#ifndef __MAINMENULAYER_H__
#define __MAINMENULAYER_H__

#include "cocos2d.h"

class MainMenuLayer : public NS_CC::Layer {
public:
    MainMenuLayer();

    static NS_CC::Scene *createScene();

    virtual bool init() override;

    CREATE_FUNC(MainMenuLayer);
private:
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event) override;
};

#endif // __MAINMENULAYER_H__
