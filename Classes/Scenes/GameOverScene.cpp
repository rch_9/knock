#include "GameOverScene.h"
#include "Managers/SceneManager.h"

USING_NS_CC;

bool GameOverLayer::init() {
    if (!Layer::init()) {
        return false;
    }
    setKeyboardEnabled(true);

    //FIXME
    auto sp = Sprite::create("menu/black_background.png");

    auto visibleSize =  Director::getInstance()->getVisibleSize();
    sp->setPosition(visibleSize.width / 2.f, visibleSize.height / 2.f);    
    sp->setOpacity(128);
    this->addChild(sp);

    MenuItemFont::setFontName("fonts/arial.ttf");

//    auto SceneManager::getInstance()->getGameLayer()->getLe
    char buffer[11];
    sprintf (buffer, "Level %d / 4", SceneManager::getInstance()->getGameLayer()->getLevel());
    auto level = Label::createWithTTF(buffer, "fonts/arial.ttf", 70);
    sp->addChild(level);
    level->setPosition(200, 1680);

    auto restart = MenuItemImage::create("menu/restart.png", "menu/restart_selected.png", [&](Ref* sender) {
            SceneManager::getInstance()->goToRestart();
});        
    auto quit = MenuItemImage::create("menu/down.png", "menu/down_selected.png", [&](Ref* sender) {
            SceneManager::getInstance()->goToMainMenuScene();
});

    auto menu = Menu::create(restart, quit, nullptr);
    menu->alignItemsVerticallyWithPadding(150);
    this->addChild(menu);

    return true;
}

//void GameOverLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event) {
//    if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE) {
//        SceneManager::getInstance()->goToMainMenuScene();
//    }
//}


void GameOverLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event) {
    if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE) {
        SceneManager::getInstance()->goToMainMenuScene();
    }
}
