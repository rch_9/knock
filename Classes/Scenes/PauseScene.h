#ifndef __PAUSELAYER_H__
#define __PAUSELAYER_H__

#include "cocos2d.h"

class PauseLayer : public cocos2d::Layer {
public:
    virtual bool init() override;

    CREATE_FUNC(PauseLayer);
private:
    void pressSettings(Ref *sender);
    void pressResume(Ref *sender);
    void pressQuit(Ref *sender);
    
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event) override;
};

#endif // __PAUSELAYER_H__
