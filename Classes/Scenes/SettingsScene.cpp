#include "SettingsScene.h"
#include "Managers/SceneManager.h"
#include "Managers/SoundManager.h"

USING_NS_CC;

bool SettingsLayer::init() {
    if (!Layer::init()) {
        return false;
    }

    setKeyboardEnabled(true);

    MenuItemFont::setFontName("fonts/arial.ttf");
    MenuItemFont::setFontSize(65);

    auto music = MenuItemFont::create("Music");    
    music->setTag(1); // tag 1
    music->setEnabled(false);
    auto vibration = MenuItemFont::create("Vibration");
    vibration->setEnabled(false);
    vibration->setTag(3); // tag 3

    auto activator = [this](const ccMenuCallback &callback, MenuItemFont* font, const bool &isActivate)->MenuItemToggle* {
        if (isActivate) {
            font->setEnabled(true);
            return MenuItemToggle::createWithCallback(callback, MenuItemImage::create("menu/button_on.png", "menu/button_on.png"),
                                                      MenuItemImage::create("menu/button_off.png", "menu/button_off.png"), nullptr);
        }
        font->setEnabled(false);
        return MenuItemToggle::createWithCallback(callback, MenuItemImage::create("menu/button_off.png", "menu/button_off.png"),
                                                  MenuItemImage::create("menu/button_on.png", "menu/button_on.png"), nullptr);
    };

    auto back = MenuItemImage::create("menu/down.png", "menu/down_selected.png", CC_CALLBACK_1(SettingsLayer::pressBack, this));

    back->setScale(0.7);

    auto musicImageToggle = activator(CC_CALLBACK_1(SettingsLayer::toggleMusicState, this), music, SoundManager::getInstance()->getIsMusicPlaying());        
    auto vibrationImageToggle = activator(CC_CALLBACK_1(SettingsLayer::toggleVibrationState, this), vibration, SoundManager::getInstance()->getIsVibrationPlaying());    

    auto visibleSize = Director::getInstance()->getWinSize();

    auto menu = Menu::create(music, musicImageToggle, vibration, vibrationImageToggle, back, nullptr);
    menu->alignItemsInColumns(2, 2, 1, nullptr);
    menu->setPosition(Vec2(visibleSize.width / 2.f, visibleSize.height / 2.f));    
    this->addChild(menu);

    return true;
}

void SettingsLayer::toggleMusicState(Ref* sender) {
    SoundManager::getInstance()->toggleMusicPlaying();
    auto item = dynamic_cast<MenuItemFont*>(dynamic_cast<MenuItemToggle*>(sender)->getParent()->getChildByTag(1));
    item->isEnabled() ? item->setEnabled(false) : item->setEnabled(true);    
}

void SettingsLayer::toggleVibrationState(Ref* sender) {    
    SoundManager::getInstance()->toggleVibrationPlaying();
    auto item = dynamic_cast<MenuItemFont*>(dynamic_cast<MenuItemToggle*>(sender)->getParent()->getChildByTag(3));
    item->isEnabled() ? item->setEnabled(false) : item->setEnabled(true);
}


void SettingsLayer::pressBack(Ref* sender) {
    static_cast<Layer*>(_parent)->setKeyboardEnabled(true);
    SceneManager::getInstance()->goFromSettingsScene(sender);
}

//void SettingsLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event) {
//    if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE) {
//        this->getParent()->getChildByTag(3)->setVisible(true); // 3 - menu
//        this->removeFromParent();
//    }
//}

void SettingsLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event) {
    if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE) {
        static_cast<Layer*>(_parent)->setKeyboardEnabled(true);
        _parent->getChildByTag(3)->setVisible(true); // 3 - menu
        this->removeFromParent();
    }
}
