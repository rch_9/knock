#include "GameScene.h"
#include "PauseScene.h"
#include "Managers/GameManager.h"
#include "Managers/SceneManager.h"
#include "ui/CocosGUI.h"
#include "../Utils.h"
#include <algorithm>

USING_NS_CC;

GameLayer::GameLayer():
    _visibleSize(Director::getInstance()->getVisibleSize()),
    _visibleOrigin(Director::getInstance()->getVisibleOrigin()),
    _rotateAngleFactor{25, 25, 1, 25},
    _speedFactor(700),
    _distanceFactor{150, 600},
    _rotateTimeFactor{0.5f, 1.f},
    _time(0),
    _level(0) {
}

Scene *GameLayer::createScene() {
    auto scene = Scene::create();
    auto layer = GameLayer::create();
    layer->setTag(0);
    scene->addChild(layer);

    return scene;
}

bool GameLayer::init() {
    if ( !Layer::init() /*Color::initWithColor(Color4B(110, 100, 100, 50))*/) {
        return false;
    }

    ui::Button* pause = ui::Button::create("menu/pause.png", "menu/pause_selected.png");
    pause->setPosition(Vec2(540.f, 1680.f));
    pause->addClickEventListener(CC_CALLBACK_1(GameLayer::pressedPause, this));
    this->addChild(pause, 99, 2); // 2 pause button

    setKeyboardEnabled(true); //?

    auto layer = Layer::create();
    layer->setTag(0);
    layer->setAnchorPoint(Vec2(0.5, 0.34));
    //    layer->keyReleased();
    addChild(layer);

    //    init background
    auto background = Sprite::createWithSpriteFrameName("background.png");
    background->setPosition(_visibleSize / 2.f);
    layer->addChild(background);

    auto background2 = Sprite::createWithSpriteFrameName("background.png");
    background2->setPosition(_visibleSize / 2.f);
    background2->setRotationSkewY(180.f);
    this->addChild(background2, -1);

    //    init player
    _playerBox = PlayerBox::create("player1_black.png");
    layer->addChild(_playerBox);
    _playerBox->setTag(1); // 1 - player box
    _playerBox->setAnchorPoint(Point::ANCHOR_BOTTOM_RIGHT);
    _playerBox->setPosition(_visibleSize.width / 2.f + _visibleOrigin.x, _playerBox->getContentSize().height);

    //    init boxes
    const int AMOUNT_BOXES = 7;
    for(int i = 0; i < AMOUNT_BOXES; ++i) {
        auto box = Box::create("box_heart_white.png");
        box->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
        box->setPosition(_visibleSize.width / 2.f, _visibleSize.height );
        box->setVisible(false);
        layer->addChild(box, 1);

        _standingBoxes.push_back(box);
    }

    scheduleUpdate();

    schedule([this](float dt){
        processTime(dt);

        auto layer = this->getChildByTag(0);
        if(!layer->getNumberOfRunningActions()) {
            int cof[3] = {RandomHelper::random_int(-_rotateAngleFactor[0], _rotateAngleFactor[0]),
                          RandomHelper::random_int(-_rotateAngleFactor[1], _rotateAngleFactor[1]),
                          RandomHelper::random_int(_rotateAngleFactor[2], _rotateAngleFactor[3])};

            float t = RandomHelper::random_real(_rotateTimeFactor[0], _rotateTimeFactor[1]);

            layer->runAction(RotateTo::create(t, Vec3(cof[0], cof[1], cof[2] * RandomHelper::random_int(-1, 1))));
        }
    }, "step_1");

    return true;
}

void GameLayer::setEnabledPlayerBox(bool flag) {
    static_cast<PlayerBox*>(this->getChildByTag(0)->getChildByTag(1))->setEnabled(flag);
}

void GameLayer::update(float dt) {
    generateBox();
    moveBoxes(dt * _speedFactor);
    processCollisions();
}

void GameLayer::generateBox() {
    if(_flyingBoxes.empty() || (_visibleSize.height - _flyingBoxes.back()->getBoundingBox().getMaxY()) >
            RandomHelper::random_int(_distanceFactor[0], _distanceFactor[1])) {
        if(!_standingBoxes.empty()) {
            auto boxAttributes = GameManager::getInstance()->generateNextBoxAttributes();
            _standingBoxes.back()->setAttributes(boxAttributes.first, boxAttributes.second);

            _flyingBoxes.push_back(_standingBoxes.back());
            _standingBoxes.back()->setVisible(true);
            _standingBoxes.pop_back();
        }
    }
}

void GameLayer::processCollisions() {
    auto playerBoundingBox = _playerBox->getBoundingBox();

    for(auto it = _flyingBoxes.begin(); it != _flyingBoxes.end(); ++it) {
        auto boxBoundingBox = (*it)->getBoundingBox();

        if(boxBoundingBox.getMaxY() < 0.f || (boxBoundingBox.getMinY() < playerBoundingBox.getMaxY()
                                              && boxBoundingBox.getMaxY() > playerBoundingBox.getMinY()
                                              && (*it)->getAnchorPoint() == _playerBox->getAnchorPoint())) {
            if(!(boxBoundingBox.getMaxY() < 0.f)) {
                GameManager::getInstance()->processCollisions(*it);
            }

            removeBox(it);
        }
    }
}

void GameLayer::processTime(const float &dt) {
    _time += dt;
    if (_level == 0) {

        if (_time < 5.7f) {
            setFactors(25, 25, 1, 35, 0.5, 1, 250, 640, dt * 5);
        } else if (_time < 22.f) {
            setFactors(45, 45, 1, 160, 1.1, 1.5, 200, 550, dt * 10);
        } else if (_time < 38) {
            setFactors(35, 35, 1, 90, 0.8, 1.1, 200, 550, dt * 20);
        } else if (_time < 70) {
            setFactors(35, 35, 1, 35, 0.3, 0.5, 190, 550, dt * 7);
        } else if (_time < 100) {
            setFactors(60, 60, 1, 180, 1.f, 2.f, 190, 500, dt * 1.5);
        } else if (_time < 117) {
            setFactors(30, 30, 1, 180, 2, 3, 180, 450, 0);
        } else if (_time < 133) {
            setFactors(60, 60, 1, 180, 1.f, 4.f, 180, 450, -dt * 1.5);
        } else if (_time < 153) {
            setFactors(45, 45, 1, 160, 2, 3, 190, 450, dt * 7.f);
        } else if (_time < 180) {
            setFactors(15, 15, 1, 180, 0.7, 1.3f, 160, 400, -dt * 15.f);
        } else if (_time < 190) {
            setFactors(10, 10, 1, 120, 1.f, 1.5f, 170, 400, -dt * 7.f);
        } else if  (_time < 199) {
            setFactors(5, 5, 1, 5, 1.2, 1.5, 190, 500, -dt * 5.f);
        } else {
            GameManager::getInstance()->putLevel(++_level);
        }
    } else if (_level == 1) {
        if (_time < 202.2f) {
            setFactors(5, 5, 1, 5, 0.3, 0.5, 300, 700, dt * 15.f);
        } else if (_time < 218) {
            setFactors(35, 35, 1, 90, 0.4, 0.6, 200, 500, dt * 6.f);
        } else if (_time < 232) {
            setFactors(55, 55, 1, 25, 0.4, 0.6, 170, 500, dt * 6.f);
        } else if (_time < 246.5) {
            setFactors(45, 45, 1, 110, 0.5, 0.6, 190, 500, dt * 6.f);
        } else if (_time < 260) {
            setFactors(60, 60, 1, 180, 1, 1.3f, 270, 600, dt * 40.f);
        } else if (_time < 275) {
            setFactors(60, 60, 1, 180, 1, 1.3, 270, 600, dt * 25.f);
        } else if (_time < 290) {
            setFactors(60, 60, 1, 110, 1, 1.3f, 320, 650, dt * 15.f);
        } else if (_time < 293) {
            setFactors(15, 15, 1, 15, 0.2, 0.3, 250, 550, -dt * 300.f);
        } else if (_time < 309) {
            setFactors(35, 35, 1, 90, 0.4, 0.5, 270, 550, dt * 4.f);
        } else if (_time < 322) {
            setFactors(45, 45, 1, 25, 0.3, 0.4, 250, 500, dt * 4.f);
        } else if (_time < 340) {
            setFactors(45, 45, 1, 180, 1.f, 2, 230, 550, dt * 15.f);
        } else if (_time < 370) {
            setFactors(60, 60, 1, 60, 0.4, 0.6f, 250, 550, dt * 1.f);
        } else if (_time < 384) {
            setFactors(35, 35, 1, 110, 0.6, 8.f, 250, 600, dt * 15.f);
        } else if (_time < 399) {
            setFactors(35, 35, 1, 50, 0.3, 0.5, 270, 550,  dt * 5.f);
        } else {
            GameManager::getInstance()->putLevel(++_level);
        }
    } else if (_level == 2) {
        if (_time < 402.3) {
            setFactors(15, 15, 1, 45, 0.2, 0.5, 270, 700, dt * -300.f);
        } else if (_time < 406) {
            setFactors(15, 15, 1, 180, 0.7, 0.8, 270, 600, dt * 45.f);
        } else if (_time < 419) {
            setFactors(30, 30, 1, 180, 0.5, 0.7, 270, 650, dt * 12.f);
        } else if (_time < 432) {
            setFactors(45, 45, 1, 70, 0.6, 0.8f, 300, 650, dt * 15.f); // 1-160
        } else if (_time < 446) {
            setFactors(15, 15, 1, 15, 0.05, 0.1, 220, 350, dt * 1.f);
        } else if (_time < 459) {
            setFactors(40, 40, 1, 60, 0.2, 0.4, 220, 470, dt * 15.f);
        } else if (_time < 486) {
            setFactors(60, 60, 1, 180, 0.8, 0.9, 250, 600, dt * 7.f);
        } else if (_time < 500.5) {
            setFactors(40, 40, 1, 90, 0.3, 0.4 , 270, 570, dt * 1.f);
        } else if (_time < 513.7) {
            setFactors(30, 30, 1, 15, 0.1, 0.5, 300, 650, dt * 12.f);
        } else if (_time < 541) {
            setFactors(60, 60, 1, 180, 0.8, 1.f, 350, 700, dt * 0.5f);
        } else if (_time < 554.5) {
            setFactors(30, 30, 1, 60, 0.1, 0.3 , 230, 570, dt * 10.f);
        } else if (_time < 568) {
            setFactors(10, 10, 1, 20, 0.7, 1.f, 220, 550, -dt * 110.f);
        } else {
            GameManager::getInstance()->putLevel(++_level);
        }
    } else if (_level == 3) {
        if (_time < 585) {
            setFactors(25, 25, 1, 140, 1.5, 2, 180, 300, -dt * 10.f);
        } else if (_time < 604.5) {
            setFactors(15, 15, 1, 180, 2, 2.5f, 150, 200, dt * 0.f);
        } else if (_time < 623) {
            setFactors(60, 60, 1, 180, 2, 2.5f, 150, 180, dt * 0.f);
        } else if (_time < 701) {
            setFactors(15, 15, 1, 180, 2, 2.5f, 150, 170, dt * 0.f);
        } else if (_time < 735) {
            setFactors(60, 60, 1, 180, 2, 2.5f, 145, 160, dt * 0.f);
        } else {
            GameManager::getInstance()->putLevel(++_level);
        }
    } else {
        SceneManager::getInstance()->goToVictoryScene();
    }

}

void GameLayer::removeBox(std::deque<Box*>::iterator box) {
    (*box)->setPositionY(_visibleSize.height);
    (*box)->setVisible(false);
    _standingBoxes.push_back(*box);
    box = _flyingBoxes.erase(box);
}

void GameLayer::setFactors(const int &p1, const int &p2, const int &p3, const int &p4, const float &p5, const float &p6, const int &p7, const int &p8, const float &p9) {
    _rotateAngleFactor[0] = p1;
    _rotateAngleFactor[1] = p2;
    _rotateAngleFactor[2] = p3;
    _rotateAngleFactor[3] = p4;
    _rotateTimeFactor[0] = p5;
    _rotateTimeFactor[1] = p6;
    _distanceFactor[0] = p7;
    _distanceFactor[1] = p8;
    _speedFactor += p9;
}

//void GameLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event) {
//    if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE) {
//        SceneManager::getInstance()->goToPauseScene();
//    }
//}


void GameLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event) {
    if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE) {
        pressedPause(this);
    }
}

const int &GameLayer::getLevel() const {
    return _level;
}

void GameLayer::moveBoxes(float dt) {
    for(auto box : _flyingBoxes) {
        box->move(dt);
    }
}


void GameLayer::pressedPause(Ref *sender) {
    setKeyboardEnabled(false);
    SceneManager::getInstance()->goToPauseScene();
}
