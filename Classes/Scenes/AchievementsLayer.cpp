#include "AchievementsLayer.h"
#include "Managers/GameManager.h"
#include "string"
#include "Managers/SceneManager.h"
#include "MainMenuScene.h"

USING_NS_CC;

bool AchievementsLayer::init() {
    if (!Layer::init()) {
        return false;
    }
//    /static_cast<MainMenuLayer*>(_parent)->setKeyboardEnabled(false);
//    _parent->getAnchorPoint();
//    getParent()
    setKeyboardEnabled(true);

    auto sp = Sprite::create("menu/black_background.png");

    auto visibleSize =  Director::getInstance()->getVisibleSize();
    sp->setPosition(visibleSize.width / 2.f, visibleSize.height / 2.f);
    this->addChild(sp);

    MenuItemFont::setFontName("fonts/arial.ttf");

    std::string wisdom;

    switch (GameManager::getInstance()->getMaxLevel()) {
    case 0:
        wisdom = "Ноль без палочки";
        break;
    case 1:
        wisdom = "Один раз не в счет";
        break;
    case 2:
        wisdom = "Как две капли воды";
        break;
    case 3:
        wisdom = "Третьего дня позавчера";
        break;
    default:
        wisdom = "Добрый конец всему делу венец";
        break;
    }

    auto level = Label::createWithTTF(wisdom, "fonts/arial.ttf", 65);
    sp->addChild(level);
    level->setPosition(540, 1600);

    auto quit = MenuItemImage::create("menu/down.png", "menu/down_selected.png", [&](Ref* sender) {
            static_cast<MainMenuLayer*>(_parent)->setKeyboardEnabled(true);
            this->removeFromParent();
});

    auto menu = Menu::create(quit, nullptr);
    menu->alignItemsVerticallyWithPadding(150);
    this->addChild(menu);

    return true;
}

void AchievementsLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event) {
    if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE) {
        static_cast<MainMenuLayer*>(_parent)->setKeyboardEnabled(true);
        this->removeFromParent();
    }
}
