#ifndef __SETTINGSLAYER_H__
#define __SETTINGSLAYER_H__

#include "cocos2d.h"

class SettingsLayer : public cocos2d::Layer {
public:
    virtual bool init() override;

    CREATE_FUNC(SettingsLayer);
private:
    void toggleMusicState(Ref *sender);    
    void toggleVibrationState(Ref *sender);    

    void pressBack(Ref *sender);
    
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event) override;
};

#endif // __SETTINGSLAYER_H__
