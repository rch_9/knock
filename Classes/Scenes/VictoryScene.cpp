#include "VictoryScene.h"
#include "Managers/SceneManager.h"

USING_NS_CC;

bool VictoryLayer::init() {
    if (!Layer::init()) {
        return false;
    }

    setKeyboardEnabled(true);

    auto sp = Sprite::create("menu/black_background.png");

    auto visibleSize =  Director::getInstance()->getVisibleSize();
    sp->setPosition(visibleSize.width / 2.f, visibleSize.height / 2.f);
    this->addChild(sp);

    MenuItemFont::setFontName("fonts/arial.ttf");

    auto level = Label::createWithTTF("Каково начало, таков и конец.", "fonts/arial.ttf", 70);
    sp->addChild(level);
    level->setPosition(540, 1600);

    auto restart = MenuItemImage::create("menu/restart.png", "menu/restart_selected.png", [&](Ref* sender) {
            SceneManager::getInstance()->goToRestart();
});
    auto quit = MenuItemImage::create("menu/down.png", "menu/down_selected.png", [&](Ref* sender) {
            SceneManager::getInstance()->goToMainMenuScene();
});

    auto menu = Menu::create(restart, quit, nullptr);
    menu->alignItemsVerticallyWithPadding(150);
    this->addChild(menu);

    return true;
}

void VictoryLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event) {
    if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE) {
//        SceneManager::getInstance()->goToMainMenuScene();
        CCLOG("victory");
    }
}
