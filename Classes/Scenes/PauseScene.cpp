#include "PauseScene.h"
#include "Managers/SceneManager.h"
#include "Scenes/SettingsScene.h"
//#include "GameScene.h"

USING_NS_CC;

bool PauseLayer::init() {
    if (!Layer::init()) {
        return false;
    }

    setKeyboardEnabled(true);

    auto sp = Sprite::create("menu/black_background.png");

    auto visibleSize =  Director::getInstance()->getVisibleSize();
    sp->setPosition(visibleSize.width / 2.f, visibleSize.height / 2.f);
//    sp->setScaleX(2.f);
    sp->setOpacity(128);
    this->addChild(sp);

    auto settings = MenuItemImage::create("menu/settings.png", "menu/settings_selected.png", CC_CALLBACK_1(PauseLayer::pressSettings, this));
    auto resume = MenuItemImage::create("menu/play.png", "menu/play_selected.png", CC_CALLBACK_1(PauseLayer::pressResume, this));
    auto quit = MenuItemImage::create("menu/down.png", "menu/down_selected.png", CC_CALLBACK_1(PauseLayer::pressQuit, this));

    auto menu = Menu::create(settings, resume, quit, nullptr);    
    menu->setPosition(visibleSize / 2.f);
    menu->alignItemsVerticallyWithPadding(visibleSize.height / 6.f);
    this->addChild(menu, 3, 3); // 3 - menu

    return true;
}

void PauseLayer::pressSettings(Ref *sender) {
    setKeyboardEnabled(false);
    SceneManager::getInstance()->goToSettingsScene(sender);
}

//    should fix
void PauseLayer::pressResume(Ref *sender) {    
    static_cast<Layer*>(_parent)->setKeyboardEnabled(true);
    SceneManager::getInstance()->goToGameSceneFromPauseScene(sender);
}

void PauseLayer::pressQuit(Ref *sender) {
    SceneManager::getInstance()->goToMainMenuScene();
}


void PauseLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event) {
    if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE) {
        static_cast<Layer*>(_parent)->setKeyboardEnabled(true);
        SceneManager::getInstance()->goToGameSceneFromPauseScene(event);
    }
}
