#include "AppDelegate.h"
#include "Scenes/MainMenuScene.h"
#include "Managers/SceneManager.h"
#include "Managers/SdkboxManager.h"
#include "Managers/SoundManager.h"

USING_NS_CC;

static cocos2d::Size designResolutionSize = cocos2d::Size(1080, 1920);

AppDelegate::AppDelegate() {
}

AppDelegate::~AppDelegate() {
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs() {
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    //GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};
    GLContextAttrs glContextAttrs = {5, 6, 5, 0, 16, 0};

    GLView::setGLContextAttrs(glContextAttrs);
}

// If you want to use packages manager to install more packages,
// don't modify or remove this function
static int register_all_packages() {
    return 0; //flag for packages manager
}

bool AppDelegate::applicationDidFinishLaunching() {
    SdkboxManager::getInstance()->init();
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) || (CC_TARGET_PLATFORM == CC_PLATFORM_MAC) || (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
      glview = GLViewImpl::createWithRect("twoTone", Rect(0, 0, 1080, 1920), 0.5);
      //glview = GLViewImpl::createWithRect("knock", Rect(0, 0, 1080, 1920), 0.3);
//      glview = GLViewImpl::createWithRect("knock", Rect(0, 0, 540, 960), 0.5);
#else
//      glview = GLViewImpl::create("twoTone");
      glview = GLViewImpl::createWithFullScreen("twoTone");
#endif
      director->setOpenGLView(glview);
  }

    // turn on display FPS
    director->setDisplayStats(false);

    // turn on display FPS
    // director->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60.0);    

    // Set the design resolution
    glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, ResolutionPolicy::SHOW_ALL);
    register_all_packages();

    director->runWithScene(MainMenuLayer::createScene());

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();
    
    SceneManager::getInstance()->goToPauseScene();
    // if you use SimpleAudioEngine, it must be pause
    // SoundManager::getInstance()->pauseBackgroundMusic();
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SoundManager::getInstance()->resumeBackgroundMusic();
}
