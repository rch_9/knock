#ifndef __KNOCK_UTILS_H__
#define __KNOCK_UTILS_H__

#include <string>
#include "cocos2d.h"

enum class TAGS_GLOSSARY {
    PLAYER_BOX = 1,
    PAUSE_BUTTON = 2,
    MENU = 3,
    PAUSE_LAYER = 4
};

enum class ZORDER_GLOSSARY {
    BACKGROUND = 0,
    FIRST_LAYER = 1,
    SECOND_LAYER = 2,
    BUTTONS = 10
};

enum class GAME_PROCESS_STATES {
    OK,
    WIN
};

namespace KnockUtils {

//статическое связывание?

static const char* DATABASE_NAME = "db.plist";

static inline std::string converIntToString(int arg) {
    char result[10];
    sprintf(result, "%d", arg);

    return result;
}

static inline bool converStringToBool(std::string arg) {
    return arg.compare("true") == 0;
}

static inline int converStringToInt(std::string arg) {
    return atoi(arg.c_str());
}

static inline int converStringToFloat(std::string arg) {
    return atof(arg.c_str());
}

cocos2d::__Dictionary* getDictionary();

bool initDictionary();

}

#endif // __KNOCK_UTILS_H__
