LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos/audio/include)
$(call import-add-path,$(LOCAL_PATH))

LOCAL_MODULE := MyGame_shared

LOCAL_MODULE_FILENAME := libMyGame

LOCAL_SRC_FILES := hellocpp/main.cpp \
../../Classes/AppDelegate.cpp \
../../Classes/Utils.cpp \
../../Classes/Scenes/GameScene.cpp \
../../Classes/Scenes/MainMenuScene.cpp \
../../Classes/Scenes/PauseScene.cpp \
../../Classes/Scenes/GameOverScene.cpp \
../../Classes/Scenes/SettingsScene.cpp \
../../Classes/Scenes/AchievementsLayer.cpp \
../../Classes/Scenes/VictoryScene.cpp \
../../Classes/Models/Box.cpp \
../../Classes/Models/PlayerBox.cpp \
../../Classes/Managers/GameManager.cpp \
../../Classes/Managers/SceneManager.cpp \
../../Classes/Managers/SoundManager.cpp \
../../Classes/Managers/DictionaryManager.cpp \
../../Classes/Managers/SdkboxManager.cpp

LOCAL_CPPFLAGS := -DSDKBOX_ENABLED
LOCAL_LDLIBS := -landroid \
-llog
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes
LOCAL_WHOLE_STATIC_LIBRARIES := PluginSdkboxPlay \
sdkbox \
PluginReview \
PluginAppodeal

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END


LOCAL_STATIC_LIBRARIES := cocos2dx_static

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)
$(call import-module, ./sdkbox)
$(call import-module, ./pluginsdkboxplay)
$(call import-module, ./pluginreview)
$(call import-module, ./pluginappodeal)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END
